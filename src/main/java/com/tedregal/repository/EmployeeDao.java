package com.tedregal.repository;

import com.tedregal.domain.Employee;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.springframework.stereotype.Repository;

@Repository
public class EmployeeDao {

    public List<Employee> buildEmployees() {
    return Stream.of(
      new Employee(1, "diego", 23),
      new Employee(2, "andrea", 22),
      new Employee(3, "abel", 27),
      new Employee(4, "maritza", 50),
      new Employee(3, "jorge", 50)
    ).collect(Collectors.toList());
  }

}
