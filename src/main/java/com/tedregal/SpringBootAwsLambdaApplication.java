package com.tedregal;

import com.tedregal.domain.Employee;
import com.tedregal.repository.EmployeeDao;
import java.util.List;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class SpringBootAwsLambdaApplication {

  @Autowired
  private EmployeeDao employeeDao;

  @Bean
  public Supplier<List<Employee>> employees() {
    return () -> employeeDao.buildEmployees();
  }

  @Bean
  public Function<String, List<Employee>> employeeByName() {
    return (input) -> employeeDao.buildEmployees().stream()
      .filter(employee -> employee.getName().equals(input)).collect(
        Collectors.toList());
  }

  public static void main(String[] args) {
    SpringApplication.run(SpringBootAwsLambdaApplication.class, args);
  }

}
